


## Creating a new MKDocs project

```
docker run --rm -v ${PWD}:/mkdoc_home fluent/mkdocs mkdocs new fluent-project
```


## Running the MKDocs server

```
cd mydocs
docker run --rm -v ${PWD}:/mkdoc_home -p 8000:8000 fluent/mkdocs

```


## To Build project

```
docker run --rm -v ${PWD}:/mkdoc_home fluent/mkdocs mkdocs build --clean

```
