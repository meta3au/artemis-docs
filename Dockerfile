FROM alpine

LABEL name="FluentCommerce MKDocs"
LABEL maintainer="jeff.chen@fluentcommerce.com"
LABEL version="0.17.5"

## Install python and pip
RUN apk add --update python py-pip
RUN pip install --upgrade pip

RUN mkdir /mkdoc_home
WORKDIR /mkdoc_home

## Install mkdocs and theme support
RUN pip install mkdocs
RUN pip install mkdocs-material
RUN pip install pygments
RUN pip install pymdown-extensions

EXPOSE 8000

CMD ["mkdocs", "serve", "--dev-addr=0.0.0.0:8000"]

